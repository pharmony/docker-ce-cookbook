# frozen_string_literal: true

module DockerCeCookbook
  module AptPackageHelper
    def build_docker_version
      expected_version = node[:docker_ce][:version]

      detected = detect_docker_ce_version_from(expected_version)

      # Return the given version from the attributes when no version found
      expected_version if detected.nil? || detected.empty?

      # Returns the first detected Docker version
      Chef::Log.info "docker-ce: Using Docker package version #{detected} ..."
      detected
    end

    def current_docker_version
      shell_out(
        "dpkg -s docker-ce 2>/dev/null | grep Version | awk '{print $2}'"
      ).stdout.gsub(/\n/, '')
    end

    def packages_to_be_installed
      case node['platform']
      when 'debian'
        case node_distribution
        when 'jessie' then ['docker-ce']
        when 'stretch', 'buster'
          package_list = ['docker-ce', 'containerd.io']

          if node['docker_ce']['version'] == 'latest' ||
             node['docker_ce']['version'].to_f >= 18.09
            package_list << 'docker-ce-cli'
          end

          package_list
        else
          fatal_unsupported_linux_distribution!
        end
      when 'ubuntu'
        case node_distribution
        when 'bionic', 'focal'
          package_list = ['docker-ce', 'containerd.io']

          if node['docker_ce']['version'] == 'latest' ||
             node['docker_ce']['version'].to_f >= 18.09
            package_list << 'docker-ce-cli'
          end

          package_list
        else
          fatal_unsupported_linux_distribution!
        end
      else
        fatal_unsupported_linux_distribution!
      end
    end

    def upgrading_docker?
      expected_version = build_docker_version
      current_version = current_docker_version

      return false if current_version.empty?

      return false if expected_version == current_version

      true
    end

    private

    # Gets all the available versions, makes a table of it, and returns only the
    # version column (Column with index 1)
    def available_docker_ce_versions
      @available_docker_ce_versions ||= begin
        versions = shell_out('apt-cache madison docker-ce').stdout.split("\n")
        versions.map { |version| version.split(' | ')[1] }
      end
    end

    #
    # Try to match, with a regex, a version and return it, otherwise show a
    # warning message and the available versions.
    #
    def detect_docker_ce_version_from(expected_version)
      versions = available_docker_ce_versions

      return versions.first if expected_version == 'latest'

      detected = versions.detect { |version| version =~ /#{expected_version}/ }

      unless detected
        Chef::Log.warn 'docker-ce: WARNING: The Docker CE version you asked ' \
                       "\"#{expected_version}\" couldn't be found, the " \
                       'install command will fail. Here are the available ' \
                       "version:\n\n#{versions.join("\n")}"
      end

      detected
    end

    def fatal_unsupported_linux_distribution!
      Chef::Log.fatal "docker-ce: This cookbook doesn't support installing " \
                      "Docker on #{node['platform']} #{node_distribution}."
      Chef::Log.fatal 'docker-ce: Please open an issue or submit a MR to the ' \
                      'cookbook repository in order support Debian ' \
                      "#{node_distribution} at " \
                      'https://gitlab.com/pharmony/docker-ce-cookbook.'

      raise
    end
  end
end
