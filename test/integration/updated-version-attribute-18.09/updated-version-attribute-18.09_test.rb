# frozen_string_literal: true

# Inspec test for recipe docker-ce-cookbook::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# The apt-transport-https package should be installed
describe package('apt-transport-https') do
  it { should be_installed }
end

# The ca-certificates package should be installed
describe package('ca-certificates') do
  it { should be_installed }
end

# The curl package should be installed
describe package('curl') do
  it { should be_installed }
end

# The gnupg2 package should be installed
describe package('gnupg2') do
  it { should be_installed }
end

# The software-properties-common package should be installed
describe package('software-properties-common') do
  it { should be_installed }
end

# The Docker APT key should present
describe command(
  'apt-key adv --list-public-keys  --with-fingerprint --with-colons'
) do
  its('stdout') { should match(/9DC858229FC7DD38854AE2D88D81803C0EBFCD88/) }
end

describe file('/etc/apt/sources.list.d/docker.list') do
  it { should exist }
  its('content') do
    should match(%r{https://download.docker.com/linux/(debian|ubuntu)})
  end
end

# The docker-ce package should be installed
describe package('docker-ce') do
  it { should be_installed }
  it { should be_held }
end

# Debian jessie doesn't have docker-ce-cli nor containerd.io
if os[:release].start_with?('8')
  # The docker-ce-cli package should not be installed
  describe package('docker-ce-cli') do
    it { should_not be_installed }
  end

  # The containerd.io package should not be installed
  describe package('containerd.io') do
    it { should_not be_installed }
  end
else
  # The docker-ce-cli package should be installed
  describe package('docker-ce-cli') do
    it { should be_installed }
    it { should be_held }
  end

  # The containerd.io package should be installed
  describe package('containerd.io') do
    it { should be_installed }
    it { should be_held }
  end
end
